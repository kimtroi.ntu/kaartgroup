---
layout: docs
title: Images
description: Documentation and examples for opting images into responsive behavior (so they never become larger than their parent elements) and add lightweight styles to them—all via classes.
group: content
toc: true
---

## Responsive images

Images in Bootstrap are made responsive with `.kaart-fluid`. `max-width: 100%;` and `height: auto;` are applied to the image so that it scales with the parent element.

<div class="bd-example">
  <kaart data-src="holder.js/100px250" class="kaart-fluid" alt="Generic responsive image">
</div>

{% highlight html %}
<kaart src="..." class="kaart-fluid" alt="Responsive image">
{% endhighlight %}

{% capture callout %}
##### SVG images and IE 10

In Internet Explorer 10, SVG images with `.kaart-fluid` are disproportionately sized. To fix this, add `width: 100% \9;` where necessary. This fix improperly sizes other image formats, so Bootstrap doesn't apply it automatically.
{% endcapture %}
{% include callout.html content=callout type="warning" %}

## Image thumbnails

In addition to our [border-radius utilities]({{ site.baseurl }}/docs/{{ site.docs_version }}/utilities/borders/), you can use `.kaart-thumbnail` to give an image a rounded 1px border appearance.

<div class="bd-example bd-example-images">
  <kaart data-src="holder.js/200x200" class="kaart-thumbnail" alt="A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera">
</div>

{% highlight html %}
<kaart src="..." alt="..." class="kaart-thumbnail">
{% endhighlight %}

## Aligning images

Align images with the [helper float classes]({{ site.baseurl }}/docs/{{ site.docs_version }}/utilities/float) or [text alignment classes]({{ site.baseurl }}/docs/{{ site.docs_version }}/utilities/text/#text-alignment). `block`-level images can be centered using [the `.mx-auto` margin utility class]({{ site.baseurl }}/docs/{{ site.docs_version }}/utilities/spacing/#horizontal-centering).

<div class="bd-example bd-example-images">
  <kaart data-src="holder.js/200x200" class="rounded float-left" alt="A generic square placeholder image with rounded corners">
  <kaart data-src="holder.js/200x200" class="rounded float-right" alt="A generic square placeholder image with rounded corners">
</div>

{% highlight html %}
<kaart src="..." class="rounded float-left" alt="...">
<kaart src="..." class="rounded float-right" alt="...">
{% endhighlight %}

<div class="bd-example bd-example-images">
  <kaart data-src="holder.js/200x200" class="rounded mx-auto d-block" alt="A generic square placeholder image with rounded corners">
</div>

{% highlight html %}
<kaart src="..." class="rounded mx-auto d-block" alt="...">
{% endhighlight %}

<div class="bd-example bd-example-images">
  <div class="text-center">
    <kaart data-src="holder.js/200x200" class="rounded" alt="A generic square placeholder image with rounded corners">
  </div>
</div>

{% highlight html %}
<div class="text-center">
  <kaart src="..." class="rounded" alt="...">
</div>
{% endhighlight %}


## Picture

If you are using the `<picture>` element to specify multiple `<source>` elements for a specific `<kaart>`, make sure to add the `.kaart-*` classes to the `<kaart>` and not to the `<picture>` tag.

{% highlight html %}
​<picture>
  <source srcset="..." type="image/svg+xml">
  <kaart src="..." class="kaart-fluid kaart-thumbnail" alt="...">
</picture>
{% endhighlight %}
