(function () {
    var detail_button, leftDetailInfoWrap;
    $(document).ready(function () {
        detail_button = $('#detail_button');
        leftDetailInfoWrap = $('#left-detail-info-wrap');
        if (detail_button.length){
            detail_button.on('click', function () {
                if (leftDetailInfoWrap.hasClass('show')){
                    leftDetailInfoWrap.removeClass('show');
                }else{
                    leftDetailInfoWrap.addClass('show');
                }
            });
        }
    });

    document.addEventListener('mouseup', function (e) {
        var container = $("#left-detail-info-wrap, #detail_button");

        if (!container.is(e.target) && container.has(e.target).length === 0){
            leftDetailInfoWrap.removeClass('show');
        }

    }.bind(this));

})();
