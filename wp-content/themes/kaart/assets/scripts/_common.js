(function ( $ ) {
    console.log('common injected!');
    var currentPageIndx = 1;
    var dots, pages, goToNextSectionBtn;

    function _scrollToSubPage(pageIndex) {

        var currentActiveDot = $('[data-nav-page="' + pageIndex + '"]');
        currentActiveDot.addClass('active');
        $('.vertical-dots-nav .dot').not(currentActiveDot).removeClass('active');

        $('body').removeClass('turn_blue').removeClass('turn_yellow').removeClass('turn_white').removeClass('turn_image');
        console.log('yeah');
        if ($('[data-parent-page="' + pageIndex + '"][data-slide]').length) {
            $('body').addClass('turn_' + $('[data-parent-page="' + pageIndex + '"][data-slide].active').attr('data-background'));
        } else {
            $('body').addClass('turn_' + $('[data-page="' + pageIndex + '"]').attr('data-background'));
        }

    }

    function navMenuControl() {

        $('#mod-nav .hamburger-btn').on('click',function() {
            $(this).toggleClass('active');
            $('#mod-nav .nav-menu-mb').slideToggle('400');
        });

    }

    function bannerIframeControl() {
        var banner = $('.banner-layer-video');
        if ( banner.length > 0 ) {
            var changeUrl = banner.attr("src").split("=");
            setTimeout(function () {
                banner.attr("src", "https://www.youtube.com/embed/"+changeUrl[1]+"?rel=0&controls=0&showinfo=0&autoplay=1&mute=1");
            },2000);
        }
    }

    $(document).ready(function() {

        navMenuControl();
        bannerIframeControl();

        if ($('#site-navigation .menu').length) {
            var linkElems = $('#site-navigation .menu').find('li > a');
            for (var i = 0; i < linkElems.length; i++) {
                if ($(linkElems[i]).attr('rel')) {
                    $(linkElems[i]).attr('href', $(linkElems[i]).attr('href') + '#' + $(linkElems[i]).attr('rel'));
                }
            }
        }


        goToNextSectionBtn = $('#go-to-next-page-btn');
        dots = $('.vertical-dots-nav .dot');
        if (dots.length) {
            dots.on('click', function(evt) {
                currentPageIndx = $(this).attr('data-nav-page');
                // scrollToSubPage(currentPageIndx);
                $.fn.fullpage.moveTo(currentPageIndx, 0);
            });
        }

        // adjustScrollWrapHeight();

        if ($('#fullpage').length) {
            pages = $('#fullpage .section');
            $('#fullpage').fullpage({
                dragAndMove: true,
                loopHorizontal: false,
                // slidesNavigation: false,
                // slidesNavPosition: 'bottom',
                // fixedElements: '#header, .footer',
                // fadingEffect: true,
                onLeave: function(index, nextIndex, direction) {
                    console.log('onSectionLeave');
                    // console.log('index', index);
                    // console.log('nextIndex', nextIndex);
                    // console.log('direction', direction);
                    if (direction == 'down' && nextIndex == $('#fullpage .section').length) {
                        goToNextSectionBtn.addClass('reverse');
                    } else {
                        goToNextSectionBtn.removeClass('reverse');
                    }


                    if ($('[data-parent-page="' + nextIndex + '"][data-slide]').length) {

                        if ($('[data-parent-page="' + nextIndex + '"][data-slide].active').attr('data-slide') == 0) {
                            $('[data-page="' + nextIndex + '"] .fp-controlArrow.fp-prev').addClass('hide');
                        } else {
                            $('[data-page="' + nextIndex + '"] .fp-controlArrow.fp-prev').removeClass('hide');
                        }

                        if ($('[data-parent-page="' + nextIndex + '"][data-slide].active').attr('data-slide') == ($('[data-parent-page="' + nextIndex + '"][data-slide]').length - 1)) {
                            $('[data-page="' + nextIndex + '"] .fp-controlArrow.fp-next').addClass('hide');
                        } else {
                            $('[data-page="' + nextIndex + '"] .fp-controlArrow.fp-next').removeClass('hide');
                        }

                    }

                    currentPageIndx = nextIndex;
                    scrollToSubPage(nextIndex);
                    fisrtLoadRotateDot();   
                    
                },
                afterLoad: function(anchorLink, index) {
                    console.log('afterSectionLoad');
                    
                    // console.log('anchorLink', anchorLink);
                    // console.log('index', index);

                    //TODO: detect unless navigate by anchor
                    // if (isFirstLoadContent && isUrlHasAnchor && isAnchorMatchElements){
                    if (isFirstLoadContent){
                        // do nothing, window.onLoad will do animation
                    }else{
                        console.log('other load');
                        var index_page = 'page-' + index;
                        if( $.isArray( animatedSections[index_page] ) ) {
                            if($.inArray( (0), animatedSections[index_page]) == -1 ){
                                animateSection(index, 0.3, -1);
                            }
                        }else if( $.inArray(index, animatedSections) == -1 ){
                            animateSection(index, 0.3, -1);
                        }  
                    }
                },
                onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex) {
                    var leavingSlide = $(this);
                    console.log('onSlideLeave');

                    // if (nextSlideIndex == 0){
                    //     $('[data-page="'+nextSlideIndex+'"] .fp-controlArrow.fp-prev').addClass('hide');
                    // }else{
                    //     $('[data-page="'+nextSlideIndex+'"] .fp-controlArrow.fp-prev').removeClass('hide');
                    // }
                    //
                    // if (nextSlideIndex == ($('[data-parent-page="'+index+'"][data-slide]').length - 1)){
                    //     $('[data-page="'+nextSlideIndex+'"] .fp-controlArrow.fp-next').addClass('hide');
                    // }else{
                    //     $('[data-page="'+nextSlideIndex+'"] .fp-controlArrow.fp-next').removeClass('hide');
                    // }


                    if ($('[data-parent-page="' + index + '"][data-slide]').length) {

                        if ($('[data-parent-page="' + index + '"][data-slide="' + nextSlideIndex + '"]').attr('data-slide') == 0) {
                            $('[data-page="' + index + '"] .fp-controlArrow.fp-prev').addClass('hide');
                        } else {
                            $('[data-page="' + index + '"] .fp-controlArrow.fp-prev').removeClass('hide');
                        }

                        if ($('[data-parent-page="' + index + '"][data-slide="' + nextSlideIndex + '"]').attr('data-slide') == ($('[data-parent-page="' + index + '"][data-slide]').length - 1)) {
                            $('[data-page="' + index + '"] .fp-controlArrow.fp-next').addClass('hide');
                        } else {
                            $('[data-page="' + index + '"] .fp-controlArrow.fp-next').removeClass('hide');
                        }

                    }

                    if ($('#left-detail-info-wrap').length){
                        $('#left-detail-info-wrap').removeClass('show');
                    }

                    // fp-controlArrow fp-prev
                    // console.log('leavingSlide', leavingSlide);
                    // console.log('anchorLink', anchorLink);
                    // console.log('index', index);
                    // console.log('slideIndex', slideIndex);
                    // console.log('direction', direction);
                    // console.log('nextSlideIndex', nextSlideIndex);

                    // console.log('currentPageIndx', currentPageIndx);
                    // scrollToSubPage(currentPageIndx, nextSlideIndex);
                },
                afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {
                    console.log('afterSlideLoad', slideIndex);

                    // console.log('anchorLink', anchorLink);
                    // console.log('index', index);
                    // console.log('slideAnchor', slideAnchor);
                    // console.log('slideIndex', slideIndex);
                    scrollToSubPage(currentPageIndx, slideIndex);
                    var index_page = 'page-' + index;
                    if(isFirstLoadContent){
                        //do nothing
                    } else{
                        if( $.isArray( animatedSections[index_page] ) && $.inArray( (slideIndex), animatedSections[index_page]) == -1 ) {
                            animateSection(index, 0.3, slideIndex);
                        }
                    }
                    
                }
            });
        }

        goToNextSectionBtn.on('click', function() {
            if (currentPageIndx < pages.length) {
                currentPageIndx++;
            } else {
                currentPageIndx = 1;
            }
            // $.fn.fullpage.moveSectionDown();
            $.fn.fullpage.moveTo(currentPageIndx, 0);
        });

        if ($('.keys-values .title[data-link-index]').length) {
            $('.keys-values .title[data-link-index]').on('mouseenter', function() {
                $('[data-link-index]').removeClass('active').addClass('title-unactive');
                $('[data-link-index="' + this.getAttribute('data-link-index') + '"]').removeClass('title-unactive').addClass('active');
            });
        }

        if ($('.image-item').length) {
            $('.image-item').on('mouseenter', function(evt) {
                $('.image-item .kaart-wrap kaart').not($(this).find('.kaart-wrap kaart')).removeClass('crossbrowser-filter-blur');
                if ($(this).find('.kaart-wrap kaart').length) {
                    $(this).find('.kaart-wrap kaart').addClass('crossbrowser-filter-blur');
                }
            });

            $('.image-item').on('mouseleave', function(evt) {
                $('.image-item .kaart-wrap kaart').removeClass('crossbrowser-filter-blur');
                $(this).find('.kaart-wrap kaart').removeClass('crossbrowser-filter-blur');
            });
        }

        if ($('.double-quotation-marks').length) {
            var doubleQuotationMarks = $('.double-quotation-marks');
            for (var idx = 0; idx < doubleQuotationMarks.length; idx++) {
                var paragraph = $(doubleQuotationMarks[idx]).find('p');
                if (paragraph.length) {
                    paragraph.eq(0).prepend('<span class="text-color-1">“</span>');
                    paragraph.eq(paragraph.length - 1).append('<span class="text-color-1">”</span>');
                }
            }
        }

        // initial
        scrollToSubPage(1);

        hidePageContent();

        $('.fp-controlArrow').addClass('display-none-imp opacity-hide-content');
    });

    var scrollToSubPage = _scrollToSubPage;


    function fisrtLoadRotateDot() {
        if(isFisrtLoadRotateState == true){
            $('.waiting-active').removeClass('waiting-active');
            $('.dot.active').removeClass('active').addClass('waiting-active');
        }
    }


    $(window).on('load', function() {

    var vertical_nav = $('.vertical-dots-nav');

        if(vertical_nav.length == 0){
            firstLoadDuration = 0;
        }

        setTimeout(function(){

            $('.vertical-dots-nav').addClass('show opacity-1');

            $('.dot.active').removeClass('active').addClass('waiting-active');

            setTimeout(function(){

                $('.dot.waiting-active').removeClass('waiting-active').addClass('active');

                isFisrtLoadRotateState = false;

                setTimeout(function(){

                    $('#go-to-next-page-btn').addClass('show opacity-1');

                        setTimeout(function(){
                            $('.fp-controlArrow').removeClass('display-none-imp').addClass('show opacity-1');


                            // TODO: first animation here
                    // do animation here
                            var  url = window.location.href, subUrl, anchor = 'no-anchor', slide = -1;
                            url = url.split('#');

                            if(url.length > 1) {
                                subUrl = url[1];
                                anchor = subUrl;
                                subUrl = subUrl.split('/');
                                if(subUrl.length > 1) {
                                    anchor = subUrl[0];
                                    slide = subUrl[1];
                                }
                            }
                            console.log(anchor + '|' + slide);

                            setTimeout(function(){
                                if(anchor == 'no-anchor'){
                                    animateSection(1, 0.3, -1);
                                }else {
                                    var sectionFisrtLoad = $("[data-anchor='" + anchor +"']");
                
                                    var page = $(sectionFisrtLoad).data('page');
                                    if(typeof(page) == 'number') {
                                        animateSection(page, 0.3, slide);
                                    }else {
                                        animateSection(1, 0.3, -1);
                                    }
                                }
                                isFirstLoadContent = false;

                            }, firstLoadDuration);

                    }, firstLoadDuration);

                }, firstLoadDuration );

            }, firstLoadDuration );

        }, 150);

    });
    

    function hidePageContent(){
        var orderElement = $('* [data-order]');
        for(var i = 0; i < orderElement.length; i++){
            $(orderElement[i]).css('display', 'none').addClass('opacity-hide-content').css('display', '');
        }

        $('.display-none-imp').removeClass('display-none-imp');
    }

    function animateSection(sectionIndex, duration, slideIndex){

        var element = ($('#fullpage .section').eq(sectionIndex - 1))[0];

        var slide = $(element).find('.slide');

        var subAnchor = '';

        if(slideIndex > -1){

            var slideAnimate = $(element).find('.slide').eq(slideIndex)[0];

            subAnchor = $(slideAnimate).find('[sub-anchor]');

            element = $(slideAnimate).find('* [data-order]');

            if($.inArray(animatedSections[ ('page-' + sectionIndex) ]) == -1){
                animatedSections[ ('page-' + sectionIndex) ] = [];
            }
            animatedSections[ ('page-' + sectionIndex) ].push(slideIndex);

        } else {

            if(slide.length == 1){
                console.log('animateSingle-slide');

                subAnchor = $(element).find('[sub-anchor]');

                element = $(element).find('* [data-order]');

                animatedSections.push(sectionIndex);
    
            } else {
                console.log('animateFirst in multi-slide');
                subAnchor = ($(element).find('.slide').eq(0)).find('[sub-anchor]');
                element = ($(element).find('.slide').eq(0)).find('* [data-order]');

                if($.inArray(animatedSections[ ('page-' + sectionIndex) ]) == -1){
                    animatedSections[ ('page-' + sectionIndex) ] = [];
                }
                animatedSections[ ('page-' + sectionIndex) ].push(0);
            }            
        }

        /* -------------------- */
        $element = $(element);
        subAnchor = $(subAnchor).attr('sub-anchor');
        var dur = duration;
        var max_order = 0; 

        $element.each(function(idx, el){
            max_order = $(el).data('order') > max_order ? $(el).data('order') : max_order;
            (function(dur, el, subAnchor){
                var order = $(el).data('order');
                var tranDuration = order*duration*1000;

                if(subAnchor == 'finalcial' && order == 2){
                    setTimeout(function(){
                        $(el).addClass('bottom-line-active').removeClass('opacity-hide-content'); 
                    }, tranDuration);
                }
                else {
                    setTimeout(function(){
                        $(el).addClass('show opacity-1'); 
                    }, tranDuration);
                }      

            })(dur, el, subAnchor);

            /* remove all class animation when done all in section */
            if(idx == (element.length - 1) ){
                (function(element, max_order){
                    setTimeout(function(){
                        $(element).each(function(idx, el){
                            $(el).removeClass('show opacity-1 opacity-hide-content');
                        });
                    }, (max_order + 1) *duration*1000);
                })(element, max_order);
                
            }
        });
    }

    var animatedSections = [];
    var isFirstLoadContent = true;
    var isFisrtLoadRotateState = true;
    var firstLoadDuration = 300;

    var CommonJS = (function() {
        return {
            openGoogleMap: function(location) {
                console.log(location);
                window.open('https://www.google.com/maps/search/?api=1&query=' + location, '_blank');
            },
            scrollToSubPage: _scrollToSubPage,
        };
    })();

}(jQuery));


(function ( $ ) {
    $(document).ready(function () {
        var pageCareer = $('.page-template-career');
        if ( pageCareer.length > 0 ) {
            var $input = $('.field .file-input');
            if ( $input.length > 0 ) {
                $input.on('change', function(){
                    if ( $('.field .file-name').hasClass('error') ) $('.field .file-name').removeClass('error');
                    if ( $input.val() ) {
                        $('.field .file-name').addClass('active').html($input.val());
                    }
                });
            }

            $(".list__jobs .item .job__title").click(function (e) {
                var id = $(this).data('show');
                $(this).toggleClass('active');
                $("#jobs__item--"+ id).slideToggle();
            });

            $(".apply__now a").click(function(){
                var get_hieght = $(".how-can-we").offset();
                var height = get_hieght.top;
                $('body,html').animate({ scrollTop: height  }, 1000);
                return false;
            }); 

            var buttonSubmit = $(".wpcf7-submit");
            if ( buttonSubmit.length > 0 ) {
                buttonSubmit.on('click', function() {
                    var fileName = $('.field .file-name');

                    $(document).ajaxComplete(function(event, xhr, settings) {
                        var data = xhr.responseText;
                        var jsonResponse = JSON.parse(data);
                        
                        if(jsonResponse.status === 'mail_sent' || jsonResponse.status === 'validation_failed')
                        {
                            event.preventDefault(); 
                            var cvError = $(".wpcf7-form-control-wrap.cv").find('.wpcf7-not-valid-tip');
                            if ( cvError.length > 0 ) {
                                cvError.css( 'display', 'none' );
                                fileName.addClass('error active')
                                .html(cvError.text());
                            } else {
                                var $input = $('.field .file-input');
                                if ( $input.val() ) {
                                    $('.field .file-name').addClass('active').html($input.val());
                                }
                            }
                        }
                    });
                });
            }
        }

        var pageProjects = $('.page-template-projects');
        if ( pageProjects.length > 0 ) {
            $( ".fsbanner" ).each(function( index ) {
                $('#demo-'+index).fsBanner({
                    showName :true,
                }); 
            });
            
            $(".fsbanner div:first-child").trigger('click');
        }

        if( $('.how-can-we').length > 0 ) {
            var setHeight = function () {
                if($(window).width() > 1024){
                    var height = $('.how-can-we .bz-form-group:first-child').height() - 20;
                    $('.how-can-we .bz-form-contact textarea').css('height', height);
                }
                else{
                    $('.how-can-we .bz-form-contact textarea').css('height', '');
                }
            };

            setHeight();
            $(window).resize(setHeight);
        }
    });
}(jQuery));
