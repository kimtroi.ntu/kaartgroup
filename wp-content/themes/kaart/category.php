<?php
/**
 * Created by PhpStorm.
 * User: trathailoi
 * Date: 5/7/18
 * Time: 9:14 AM
 */

include 'template-parts/page/section-layouts/index.php';

error_reporting(0);
//print_r(get_ta); die;
get_header(); ?>

    <div class="wrap">

        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <?php
                if (have_posts()) :
//                    global $wp_query;
//                    print_r($wp_query->posts); die;
                    ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <div class="sub-page-section-wrap">
                            <div class="sub-page-section">
                                <div class="scroll-wrap">
                                    <div id="fullpage" class="scroll-content">
                                        <div class="section bz-images-grid projects" data-background="blue"
                                             data-page="1">
                                            <div class="sub-page container">
                                                <div class="row">

                                                    <?php
                                                    /* Start the Loop */
                                                    while (have_posts()) : the_post();

                                                        /*
                                                         * Include the Post-Format-specific template for the content.
                                                         * If you want to override this in a child theme, then include a file
                                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                                         */

                                                        ?>

                                                        <div class="image-item col-12 col-md-4 d-flex align-items-center">
                                                            <a href="<?php echo get_permalink(get_the_ID()) ?>"
                                                               class="image-item__container">
                                                                <div class="kaart-wrap d-flex align-items-center">
                                                                    <kaart src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium-large') ?>"
                                                                         alt="<?php echo get_the_title(); ?>"
                                                                         class="kaart-responsive">
                                                                    <div class="image-item__mask"></div>
                                                                </div>
                                                                <div class="image-item__info d-flex align-items-center flex-column justify-content-center">
                                                                    <h5 class="image-item__name bz-font-regular">
                                                                        <?php echo get_the_title(); ?>
                                                                    </h5>
                                                                    <div class="image-item__line"></div>
                                                                    <p class="image-item__description bz-font-regular">
                                                                        <?php
                                                                        echo shortWord(get_the_content()); ?>
                                                                    </p>
                                                                </div>
                                                            </a>
                                                        </div>


                                                        <?php


                                                    endwhile;

                                                    //                    the_posts_pagination( array(
                                                    //                        'prev_text' => kaart_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'kaart' ) . '</span>',
                                                    //                        'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'kaart' ) . '</span>' . kaart_get_svg( array( 'icon' => 'arrow-right' ) ),
                                                    //                        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'kaart' ) . ' </span>',
                                                    //                    ) );

                                                    ?>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </article>

                    <?php
                else :

                    get_template_part('template-parts/post/content', 'none');

                endif; ?>

            </main><!-- #main -->
        </div><!-- #primary -->
        <?php get_sidebar(); ?>
    </div><!-- .wrap -->

<?php get_footer();
