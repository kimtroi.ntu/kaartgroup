<?php
/**
 * Created by PhpStorm.
 * User: xuantu
 * Date: 4/13/18
 * Time: 7:38 PM
 */
function kaart_scripts() {

    /**
     *   bower vendor
     */

    /**
     *   end bower vendor
     */



    /**
     *   gulp build
     */
    $stylePath = '/assets/dist/master.min.css';
    if (WP_DEBUG === true) {
        $stylePath = '/assets/bundles/master.css';
    }
	wp_enqueue_style( 'master-css', get_theme_file_uri( $stylePath ), array(), filemtime( get_theme_file_path( $stylePath ) ) );


    $helperScriptPath = '/assets/dist/helper.min.js';
    if (WP_DEBUG === true) {
        $helperScriptPath = '/assets/bundles/helper.js';
    }
    wp_enqueue_script( 'helper-js', get_theme_file_uri( $helperScriptPath ), array(), filemtime( get_template_directory() . $helperScriptPath  ), true );

	$scriptPath = '/assets/dist/common.min.js';
    if (WP_DEBUG === true) {
        $scriptPath = '/assets/bundles/common.js';
    }
    wp_enqueue_script( 'common-js', get_theme_file_uri( $scriptPath ), array(), filemtime( get_template_directory() . $scriptPath  ), true );

    /**
     *  end gulp build
     */
}

add_action( 'wp_enqueue_scripts', 'kaart_scripts' );


function manualEmbedScript($name)
{
    $path = '/assets/dist/'.$name.'.min.js';

    if (WP_DEBUG === true) {
        $path = '/assets/bundles/'.$name.'.js';
    }

    wp_enqueue_script( $name.'-js', get_theme_file_uri( $path ), array(), filemtime( get_template_directory() . $path  ), true );
}