<?php
/**
 * kaart Holding back compat functionality
 *
 * Prevents kaart Holding from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package WordPress
 * @subpackage kaart_Holding
 * @since kaart Holding 1.0
 */

/**
 * Prevent switching to kaart Holding on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since kaart Holding 1.0
 */
function kaart_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'kaart_upgrade_notice' );
}
add_action( 'after_switch_theme', 'kaart_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * kaart Holding on WordPress versions prior to 4.7.
 *
 * @since kaart Holding 1.0
 *
 * @global string $wp_version WordPress version.
 */
function kaart_upgrade_notice() {
	$message = sprintf( __( 'kaart Holding requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'kaart' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since kaart Holding 1.0
 *
 * @global string $wp_version WordPress version.
 */
function kaart_customize() {
	wp_die( sprintf( __( 'kaart Holding requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'kaart' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'kaart_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since kaart Holding 1.0
 *
 * @global string $wp_version WordPress version.
 */
function kaart_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'kaart Holding requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'kaart' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'kaart_preview' );
