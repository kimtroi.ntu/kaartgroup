<?php 

add_action("wpcf7_before_send_mail", "wpcf7_do_something_else"); 

function wpcf7_do_something_else($cf7) {
    $wpcf = WPCF7_ContactForm::get_current();
    $contactformsubmition = WPCF7_Submission::get_instance();

    if ( $contactformsubmition ) {
        $formData = $contactformsubmition->get_posted_data();  
        $location = $formData['location'];

        if(isset($location)){
        	$co = get_field("email_for_co","option");
        	$wa = get_field("email_for_wa","option");
        	$ca = get_field("email_for_ca","option");
        	$hr = get_field("email_for_hr","option");

        	$currentformInstance  = WPCF7_ContactForm::get_current();
        	$contactformsubmition = WPCF7_Submission::get_instance();

	        if ($contactformsubmition) {
	            if($location=="Grand Junction, CO")   {
	                $send_for_email = $co;
	            }

	            if($location=="Seattle, WA")   {
	                $send_for_email = $wa;
	            }

	            if($location=="Fremont, CA")   {
	                $send_for_email = $ca;
	            }

	            $data = $contactformsubmition->get_posted_data();

	            if (empty($data))
	                return;

	            $mail = $currentformInstance->prop('mail');

	            if ( $mail['recipient'] == null ) {
	            	$mail['recipient'] = "$send_for_email";
	            } else {
	            	$mail['recipient'] .= ",$send_for_email";
	            }
	            
	            $mail['additional_headers'] = "Cc: $hr";

	            // Save the email body
	            $currentformInstance->set_properties(array(
	                "mail" => $mail
	            ));
	            // return current cf7 instance
	            return $currentformInstance;
	        }

        }
    }


    return $wpcf;
}