<?php
/**
 * kaart Holding: Customizer
 *
 * @package WordPress
 * @subpackage kaart_Holding
 * @since 1.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function kaart_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport          = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport   = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport  = 'postMessage';

	$wp_customize->selective_refresh->add_partial( 'blogname', array(
		'selector' => '.site-title a',
		'render_callback' => 'kaart_customize_partial_blogname',
	) );
	$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
		'selector' => '.site-description',
		'render_callback' => 'kaart_customize_partial_blogdescription',
	) );

	/**
	 * Custom colors.
	 */
	$wp_customize->add_setting( 'colorscheme', array(
		'default'           => 'light',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'kaart_sanitize_colorscheme',
	) );

	$wp_customize->add_setting( 'colorscheme_hue', array(
		'default'           => 250,
		'transport'         => 'postMessage',
		'sanitize_callback' => 'absint', // The hue is stored as a positive integer.
	) );

	$wp_customize->add_control( 'colorscheme', array(
		'type'    => 'radio',
		'label'    => __( 'Color Scheme', 'kaart' ),
		'choices'  => array(
			'light'  => __( 'Light', 'kaart' ),
			'dark'   => __( 'Dark', 'kaart' ),
			'custom' => __( 'Custom', 'kaart' ),
		),
		'section'  => 'colors',
		'priority' => 5,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'colorscheme_hue', array(
		'mode' => 'hue',
		'section'  => 'colors',
		'priority' => 6,
	) ) );

	/**
	 * Theme options.
	 */
	$wp_customize->add_section( 'theme_options', array(
		'title'    => __( 'Theme Options', 'kaart' ),
		'priority' => 130, // Before Additional CSS.
	) );

	$wp_customize->add_setting( 'page_layout', array(
		'default'           => 'two-column',
		'sanitize_callback' => 'kaart_sanitize_page_layout',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'page_layout', array(
		'label'       => __( 'Page Layout', 'kaart' ),
		'section'     => 'theme_options',
		'type'        => 'radio',
		'description' => __( 'When the two-column layout is assigned, the page title is in one column and content is in the other.', 'kaart' ),
		'choices'     => array(
			'one-column' => __( 'One Column', 'kaart' ),
			'two-column' => __( 'Two Column', 'kaart' ),
		),
		'active_callback' => 'kaart_is_view_with_layout_option',
	) );

	/**
	 * Filter number of front page sections in kaart Holding.
	 *
	 * @since kaart Holding 1.0
	 *
	 * @param int $num_sections Number of front page sections.
	 */
	$num_sections = apply_filters( 'kaart_front_page_sections', 4 );

	// Create a setting and control for each of the sections available in the theme.
	for ( $i = 1; $i < ( 1 + $num_sections ); $i++ ) {
		$wp_customize->add_setting( 'panel_' . $i, array(
			'default'           => false,
			'sanitize_callback' => 'absint',
			'transport'         => 'postMessage',
		) );

		$wp_customize->add_control( 'panel_' . $i, array(
			/* translators: %d is the front page section number */
			'label'          => sprintf( __( 'Front Page Section %d Content', 'kaart' ), $i ),
			'description'    => ( 1 !== $i ? '' : __( 'Select pages to feature in each area from the dropdowns. Add an image to a section by setting a featured image in the page editor. Empty sections will not be displayed.', 'kaart' ) ),
			'section'        => 'theme_options',
			'type'           => 'dropdown-pages',
			'allow_addition' => true,
			'active_callback' => 'kaart_is_static_front_page',
		) );

		$wp_customize->selective_refresh->add_partial( 'panel_' . $i, array(
			'selector'            => '#panel' . $i,
			'render_callback'     => 'kaart_front_page_section',
			'container_inclusive' => true,
		) );
	}
}
add_action( 'customize_register', 'kaart_customize_register' );

/**
 * Sanitize the page layout options.
 *
 * @param string $input Page layout.
 */
function kaart_sanitize_page_layout( $input ) {
	$valid = array(
		'one-column' => __( 'One Column', 'kaart' ),
		'two-column' => __( 'Two Column', 'kaart' ),
	);

	if ( array_key_exists( $input, $valid ) ) {
		return $input;
	}

	return '';
}

/**
 * Sanitize the colorscheme.
 *
 * @param string $input Color scheme.
 */
function kaart_sanitize_colorscheme( $input ) {
	$valid = array( 'light', 'dark', 'custom' );

	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}

	return 'light';
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @since kaart Holding 1.0
 * @see kaart_customize_register()
 *
 * @return void
 */
function kaart_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since kaart Holding 1.0
 * @see kaart_customize_register()
 *
 * @return void
 */
function kaart_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Return whether we're previewing the front page and it's a static page.
 */
function kaart_is_static_front_page() {
	return ( is_front_page() && ! is_home() );
}

/**
 * Return whether we're on a view that supports a one or two column layout.
 */
function kaart_is_view_with_layout_option() {
	// This option is available on all pages. It's also available on archives when there isn't a sidebar.
	return ( is_page() || ( is_archive() && ! is_active_sidebar( 'sidebar-1' ) ) );
}

/**
 * Bind JS handlers to instantly live-preview changes.
 */
function kaart_customize_preview_js() {
	wp_enqueue_script( 'kaart-customize-preview', get_theme_file_uri( '/assets/js/customize-preview.js' ), array( 'customize-preview' ), '1.0', true );
}
add_action( 'customize_preview_init', 'kaart_customize_preview_js' );

/**
 * Load dynamic logic for the customizer controls area.
 */
function kaart_panels_js() {
	wp_enqueue_script( 'kaart-customize-controls', get_theme_file_uri( '/assets/js/customize-controls.js' ), array(), '1.0', true );
}
add_action( 'customize_controls_enqueue_scripts', 'kaart_panels_js' );




add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
    // Get the Post ID.
//    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
//    if( !isset( $post_id ) ) return;
    // Hide the editor on the page titled 'Homepage'
//    $homepgname = get_the_title($post_id);
//    if($homepgname == 'Homepage'){
//        remove_post_type_support('page', 'editor');
//    }
    // Hide the editor on a page with a specific page template
    // Get the name of the Page Template file.
//    $template_file = get_post_meta($post_id, '_wp_page_template', true);
//    if($template_file == 'my-page-template.php'){ // the filename of the page template
//        remove_post_type_support('page', 'editor');
//    }
    if (isset($_GET['post_type'])){
        if ($_GET['post_type'] == 'page'){
            remove_post_type_support('page', 'editor');
        }
    }

}
