<?php
/**
 * Created by PhpStorm.
 * User: trathailoi
 * Date: 4/23/18
 * Time: 8:57 AM
 */

//// Custom Post Type for Project
//$blog_args = array(
//    'label'				=> __( 'projects' ),
//    'description'	=> __( '' ),
//    'labels'			=> array(
//        'name'								=> __( 'Projects' ),
//        'singular_name'				=> __( 'Project' ),
//        'menu_name'						=> __( 'Projects' ),
//        'all_items'						=> __( 'All Projects' ),
//        'view_item'						=> __( 'View Project' ),
//        'add_new_item'				=> __( 'Add New Project' ),
//        'add_new'							=> __( 'Add New' ),
//        'edit_item'						=> __( 'Edit Project' ),
//        'update_item'					=> __( 'Update Project' ),
//        'search_items'				=> __( 'Search Project' ),
//        'not_found'						=> __( 'Not Found' ),
//        'not_found_in_trash'	=> __( 'Not found in Trash' ),
//    ),
//    'supports'						=> array( 'title', 'editor', 'thumbnail' ),
//    'hierarchical'				=> false,
//    'public'							=> true,
//    'show_ui'							=> true,
//    'show_in_menu'				=> true,
//    'show_in_nav_menus'		=> true,
//    'show_in_admin_bar'		=> true,
//    'menu_icon'						=> 'dashicons-welcome-widgets-menus',
//    'menu_position'				=> 5,
//    'has_archive'					=> true,
//    'publicly_queryable'	=> true,
//    'capability_type'			=> 'page',
//    'rewrite'							=> array(
//        'slug'				=> 'project',
//        'with_front'	=> false,
//        'feeds'				=> false,
//        'pages'				=> true
//    )
//);
//
//register_post_type( 'project', $blog_args );