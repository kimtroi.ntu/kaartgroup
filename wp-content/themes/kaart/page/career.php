<?php
/**Template Name: Career Page
 */
get_header(); ?>
<div  id="mod-careers-top" class="career__block">
	<?php if( have_rows('career_headline') ):
    	while ( have_rows('career_headline') ) : the_row(); ?>
			<div class="about__team">
				<div class="container">
					<p><?=get_sub_field("description") ?></p>
				</div>
				<div class="images">
					<div class="row">
						<div class="img img-first col-4">
							<div class="img" style="background-image: url(<?= get_sub_field("image_1") ?>);">
								<img src="<?=get_sub_field("image_1") ?>">
							</div>
							<div class="info__work">
								<h4><?=get_sub_field("title_item") ?></h4>
								<p><?=get_sub_field("description_item") ?></p>
							</div>
						</div>
						
						<div class="img col-5" style="background-image: url(<?= get_sub_field("image_2") ?>);">
							<img src="<?=get_sub_field("image_2") ?>">
						</div>
						
						<div class="img col-3" style="background-image: url(<?= get_sub_field("image_3") ?>);">
							<img src="<?= get_sub_field("image_3") ?>">
						</div>
					</div>
				</div>
			</div>
		<?php endwhile;endif; ?>
	</div>
	
	<div  id="mod-careers" class="career__block">
	<?php if( have_rows('career_join_our_team') ):
    	while ( have_rows('career_join_our_team') ) : the_row(); ?>
			<div class="about__team">
				<div class="container">
					<h3><?=get_sub_field("top_title") ?></h3>
					<p><?=get_sub_field("sub_title") ?></p>
				</div>
				<div class="images">
					<div class="row">
						<div class="img col-3" style="background-image: url(<?= get_sub_field("image_1") ?>);">
							<img src="<?= get_sub_field("image_1") ?>">
						</div>
						<div class="img col-5" style="background-image: url(<?= get_sub_field("image_2") ?>);">
							<img src="<?=get_sub_field("image_2") ?>">
						</div>
						<div class="img img-last col-4">
							<div class="img" style="background-image: url(<?= get_sub_field("image_3") ?>);">
								<img src="<?=get_sub_field("image_3") ?>">
							</div>
							<div class="info__work">
								<h4><?=get_sub_field("title_item") ?></h4>
								<p><?=get_sub_field("description_item") ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile;endif; ?>

		<?php if( have_rows('career_current_jobs') ):
    		while ( have_rows('career_current_jobs') ) : the_row();?>
		<div class="current__jobs">
			<div class="container">
				<h2 class="title__jobs"><?=get_sub_field("top_title") ?></h2>
				<div class="list__jobs">
				<?php if( have_rows('list_job') ): $j=0;
    				while ( have_rows('list_job') ) : the_row();  $j++;?>
					<div class="item">
						<div class="job__title" data-show=<?=$j ?>>
							<h3><?=get_sub_field("name_job"); ?></h3>
							<span>></span>
						</div>
						
						<div class="content" id="jobs__item--<?=$j ?>">
							<div class="row">
								<?php  
									$content_right = get_sub_field("content_right");
									$size = trim($content_right["jop_type"]) != '' || trim($content_right["shift"]) != '' || trim($content_right["experience"]) != '' || trim($content_right["education"]) != '' || trim($content_right["language"]) != '' || trim($content_right["location"]) != '' ? true : false;
								?>
								<div class="<?= $size > 0 ? 'col-8' : 'col-12' ?>">
									<?=get_sub_field("content_left")?>
								</div>

								<?php if ( $size > 0 ) : ?>
								<div class="col-4">
									<?php if($content_right["jop_type"]){ ?>
									<div class="info_job">
										<p class="title">JOP TYPE:</p>
										<span><?=$content_right["jop_type"] ?></span>
									</div>
									<?php } ?>

									<?php if($content_right["shift"]){ ?>
									<div class="info_job">
										<p class="title">SHIFT</p>
										<span><?=$content_right["shift"] ?></span>
									</div>
									<?php } ?>

									<?php if($content_right["experience"]){ ?>
									<div class="info_job">
										<p class="title">EXPERIENCE:</p>
										<span><?=$content_right["experience"] ?></span>
									</div>
									<?php } ?>

									<?php if($content_right["education"]) { ?>
									<div class="info_job">
										<p class="title">EDUCATION:</p>
										<span><?=$content_right["education"] ?></span>
									</div>
									<?php } ?>

									<?php if($content_right["language"]){ ?>
									<div class="info_job">
										<p class="title">LANGUAGE:</p>
										<span><?=$content_right["language"] ?></span>
									</div>
									<?php } ?>

									<?php if($content_right["location"]){ ?>
									<div class="info_job">
										<p class="title">LOCATION:</p>
										<span><?=$content_right["location"] ?></span>
									</div>
									<?php } ?>
								
									<div class="apply__now">
										<a href="javascript:void(0)">APPLY NOW</a>
									</div>
								</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<?php endwhile;endif; ?>
				</div>
			</div>
		</div>
	<?php endwhile;endif; ?>
</div>
<?php get_footer(); ?>