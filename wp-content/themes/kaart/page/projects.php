<?php
/**Template Name: projects Page
 */

get_header(); ?>

	<div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

				<div id="mod-projects" class="projects__content--list">
					<?php if( have_rows('current_project') ):
				    	while ( have_rows('current_project') ) : the_row(); ?>
				    	
					    	<section class="top-section">
					            <div class="container top-section-container">
					            	<p class="title text-center">
				                        <?= get_sub_field("top_title") ?>
									</p>
					                <p class="description text-center">
										<?= get_sub_field("sub_title") ?>
					                </p>
					            </div>
					        </section>

				    		<?php if( have_rows('projects') ):
				    			$i=-1;
				    			while ( have_rows('projects') ) : the_row(); $i++; ?>

									<div class="items">
										<div class="include__item--images">
											<div class="fsbanner" id="demo-<?=$i?>">
													<?php if( have_rows('images') ):
														$index = 0;
				    									while ( have_rows('images') ) : the_row(); ?>
													    	<div style="background-image:url(<?=get_sub_field("image") ?>)"<?= $index == 0 ? ' class="active"' : '' ?>>
														    	<span class="name">
														    		<?= file_get_contents( site_url() . '/wp-content/themes/kaart/assets/images/view-icon.svg' ); ?>
														    	</span>
													    	</div>
												<?php $index++; endwhile; endif; ?>
										  	</div>
										</div>

										<div class="info__project">
											<div class="container ">
												<div class="row">
													<div class="name col-3">
														<h3><?=get_sub_field("title_project") ?></h3>
														<h2><?=get_sub_field("name") ?></h2>
													</div>

													<div class="description__one col-4">
														<?=get_sub_field("description_one") ?>
													</div>

													<div class="description__two col-4 col-offset-1">
														<?=get_sub_field("description_two") ?>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								<?php endwhile; endif; ?>
						<?php endwhile; ?>
				    <?php endif; ?>
				</div>
			</main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .wrap -->
<?php get_footer(); ?>
