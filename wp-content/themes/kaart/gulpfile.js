var site_theme_link = 'http://localhost/kaart/wp-content/themes/kaart';
// Include gulp
var gulp = require('gulp');
require('wiredep')({
        directory: '.bowerrc'.directory || 'assets/vendor', // default: '.bowerrc'.directory || bower_components
        bowerJson: require('./bower.json'),        // default: require('./bower.json')
        src: ['./header.php', './footer.php'],

        // ----- Advanced Configuration -----
        // All of the below settings are for advanced configuration, to
        // give your project support for additional file types and more
        // control.
        //
        // Out of the box, wiredep will handle HTML files just fine for
        // JavaScript and CSS injection.

        cwd: './',

        dependencies: true,    // default: true
        devDependencies: true, // default: false
        includeSelf: true,     // default: false

        exclude: [/jquery/, '.bowerrc'.directory +'/modernizr/modernizr.js'],

        // ignorePath: /string or regexp to ignore from the injected filepath/,

        overrides: {
            // see `Bower Overrides` section below.
            //
            // This inline object offers another way to define your overrides if
            // modifying your project's `bower.json` isn't an option.
        },

        onError: function (err) {
            // If not overridden, an error will throw.

            // err = Error object.
            // err.code can be:
            //   - "PKG_NOT_INSTALLED" (a Bower package was not found)
            //   - "BOWER_COMPONENTS_MISSING" (cannot find the `bower_components` directory)
        },

        onFileUpdated: function (filePath) {
            // filePath = 'name-of-file-that-was-updated'
        },

        onPathInjected: function (fileObject) {
            // fileObject.block = 'type-of-wiredep-block' ('js', 'css', etc)
            // fileObject.file = 'name-of-file-that-was-updated'
            // fileObject.path = 'path-to-file-that-was-injected'
        },

        onMainNotFound: function (pkg) {
            // pkg = 'name-of-bower-package-without-main'
        },

        fileTypes: {

            php: {
                block: /(([ \t]*)<!--\s*bower:*(\S*)\s*-->)(\n|\r|.)*?(<!--\s*endbower\s*-->)/gi,
                detect: {
                    js: /<script.*src=['"]([^'"]+)/gi,
                    css: /<link.*href=['"]([^'"]+)/gi
                },
                replace: {
                    js: '<script src="'+site_theme_link+'/{{filePath}}"></script>',
                    css: '<link rel="stylesheet" href="'+site_theme_link+'/{{filePath}}" />'
                }
            },
            // defaults:
            html: {
                block: /(([ \t]*)<!--\s*bower:*(\S*)\s*-->)(\n|\r|.)*?(<!--\s*endbower\s*-->)/gi,
                detect: {
                    js: /<script.*src=['"]([^'"]+)/gi,
                    css: /<link.*href=['"]([^'"]+)/gi
                },
                replace: {
                    js: '<script src="'+site_theme_link+'/{{filePath}}"></script>',
                    css: '<link rel="stylesheet" href="'+site_theme_link+'/{{filePath}}" />'
                }
            },

            sass: {
                block: /(([ \t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
                detect: {
                    css: /@import\s(.+css)/gi,
                    sass: /@import\s(.+sass)/gi,
                    scss: /@import\s(.+scss)/gi
                },
                replace: {
                    css: '@import {{filePath}}',
                    sass: '@import {{filePath}}',
                    scss: '@import {{filePath}}'
                }
            },

            scss: {
                block: /(([ \t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
                detect: {
                    css: /@import\s['"](.+css)['"]/gi,
                    sass: /@import\s['"](.+sass)['"]/gi,
                    scss: /@import\s['"](.+scss)['"]/gi
                },
                replace: {
                    css: '@import "{{filePath}}";',
                    sass: '@import "{{filePath}}";',
                    scss: '@import "{{filePath}}";'
                }
            }

        }
    }
);

var wiredep = require('wiredep').stream;

// Include Our Plugins
var jshint = require('gulp-jshint'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	postcss = require('gulp-postcss'),
	sass = require('gulp-sass'),
	autoprefixer = require('autoprefixer'),
	cssnano = require('cssnano'),
	pxtorem = require('postcss-pxtorem'),
	lost = require('lost'),
	mqpacker = require('css-mqpacker'),
	sourcemaps = require('gulp-sourcemaps'),
	rename = require('gulp-rename'),
	imagemin = require('gulp-imagemin'),
	fs = require('fs'),
	merge = require('merge-stream'),
	dotenv = require('dotenv').load()

var path = require('path')

// Use path.join() for path generation to avoid cross-platform issues.
var paths = {
	bundles: path.join('assets', 'bundles'),
	dist: path.join('assets', 'dist'),
	images: path.join('assets', 'images'),
	scripts: path.join('assets', 'scripts'),
	vendor: path.join('assets', 'vendor'),
	styles: path.join('assets', 'styles')
}


gulp.task('bower', function () {
    gulp.src([
        './header.php',
    	'./footer.php'
		])
        .pipe(wiredep({
            optional: 'configuration',
            goes: 'here'
        }))
        .pipe(gulp.dest('./'));
});


// Compile Our Styles
gulp.task('styles', function () {
  return gulp.src([
      paths.vendor + '/bootstrap/dist/css/bootstrap.min.css',
      paths.vendor + '/fullpage.js/dist/jquery.fullpage.css',
      paths.vendor + '/fsbanner/fsbanner.css',
	  paths.styles + '/*.scss',
	  paths.styles + '/*.css'
  	])
	.pipe(sourcemaps.init())
	.pipe(sass())
	.pipe(postcss([lost()]))
	.pipe(postcss([autoprefixer({
		browsers: ['last 5 versions']
	})]))
	// .pipe(postcss([pxtorem({
	// 	prop_white_list: [] // Leave Array Blank to transform all px values to rem
	// })]))
	.pipe(postcss([mqpacker()]))
	.pipe(sourcemaps.write())
	.pipe(concat('master.css'))
	.pipe(gulp.dest(paths.bundles))
	.pipe(rename('master.min.css'))
	.pipe(postcss([cssnano()]))
	.pipe(gulp.dest(paths.dist))
})

// Concatenate & Minify JS
gulp.task('scripts', function () {
	return gulp.src([
	        // paths.vendor + '/jquery/dist/jquery.js',
	        paths.vendor + '/popper.js/dist/umd/popper.js',
	        paths.vendor + '/bootstrap/dist/js/bootstrap.min.js',
	        paths.vendor + '/fullpage.js/dist/jquery.fullpage.extensions.min.js',
	        paths.vendor + '/fullpage.js/dist/jquery.fullpage.js',
            paths.vendor + '/fsbanner/fsbanner.js',
			paths.scripts + '/*.js'
		])
		.pipe(concat('common.js'))
		.pipe(gulp.dest(paths.bundles))
		.pipe(rename('common.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(paths.dist)) 
})

// Lint javascript
gulp.task('jslint', function () {
	return gulp.src(paths.scripts + '/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
})

// Concatenate & Minify JS in folders into one file per folder
gulp.task('scripts_folders', function () {
	var folders = fs.readdirSync(paths.scripts).filter(function (file) {
		return fs.statSync(path.join(paths.scripts, file)).isDirectory()
	})

	var tasks = folders.map(function (folder) {
		return gulp.src(path.join(paths.scripts, folder, '/*.js'))
			.pipe(concat(folder + '.js'))
			.pipe(gulp.dest(paths.bundles))
			.pipe(rename(folder + '.min.js'))
			.pipe(uglify())
			.pipe(gulp.dest(paths.dist))
	})

	return merge(tasks)
})

// Lint javascript
gulp.task('jslint_folders', function () {
	return gulp.src(paths.scripts + '/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
})

// Compress Images
gulp.task('images', function () {
	return gulp.src(paths.images + '/*')
		.pipe(imagemin())
		.pipe(gulp.dest(paths.images))
})

// Watch Files For Changes
gulp.task('watch', ['jslint', 'jslint_folders', 'styles', 'scripts'], function () { //, 'bower'
	gulp.watch(paths.scripts + '/*.js', ['jslint', 'scripts'])
	gulp.watch(paths.scripts + '/*/*.js', ['jslint_folders', 'scripts_folders', 'scripts'])
	gulp.watch([paths.styles + '/*.scss', paths.styles + '/*.css',paths.styles + '/*/*.scss'], ['styles'])
})

// Default Task
gulp.task('default', ['styles', 'scripts', 'scripts_folders'])
