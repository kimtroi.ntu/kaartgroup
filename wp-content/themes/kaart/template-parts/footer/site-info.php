<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage kaart_Holding
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'kaart' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'kaart' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
