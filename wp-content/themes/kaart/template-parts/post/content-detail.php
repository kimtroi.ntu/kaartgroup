<?php
/**
 * Created by PhpStorm.
 * User: trathailoi
 * Date: 5/2/18
 * Time: 6:38 PM
 */
// TODO: ráp project detail
include get_template_directory().'/template-parts/page/section-layouts/index.php';
$section = get_post($post, 'ARRAY_A');
$section['sections'] = get_field('sections', $post->ID);
?>

<?php 
    $infomations = get_field('infomations', $post->ID);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div id="left-detail-info-wrap" class="left-detail-info-wrap">
    <div class="left-detail-info-container">
        <h1 class="project-title bz-font-bold">
            <?php echo $post->post_title ?>
        </h1>
        <div class="text-content">
            <p>
            <?php echo $post->post_content ?>
            </p>
        </div>
        <?php if(isset($infomations) && ( is_array($infomations) || is_object($infomations) )) : ?>
        <div class="detail-info-table">
            <div class="info-row no-border">
                <div class="info-left bz-font-bold">Location</div>
                <div class="info-right bz-font-regular"><?php echo $infomations['location'] ?></div>
            </div>
            <div class="info-row">
                <div class="info-left bz-font-bold">Scale</div>
                <div class="info-right bz-font-regular"><?php echo $infomations['scale'] ?></div>
            </div>
            <div class="info-row">
                <div class="info-left bz-font-bold">Capital Investment</div>
                <div class="info-right bz-font-regular"><?php echo $infomations['capital_investment'] ?></div>
            </div>
            <div class="info-row">
                <div class="info-left bz-font-bold">Investor</div>
                <div class="info-right bz-font-regular"><?php echo $infomations['investor'] ?></div>
            </div>
            <div class="info-row">
                <div class="info-left bz-font-bold">Architecture</div>
                <div class="info-right bz-font-regular"><?php echo $infomations['architecture'] ?></div>
            </div>
            <div class="info-row">
                <div class="info-left bz-font-bold">Completion</div>
                <div class="info-right bz-font-regular"><?php echo $infomations['completion'] ?></div>
            </div>
        </div>
        <?php endif ?>
    </div>
</div>

<div class="sub-page-section-wrap">
    <div class="sub-page-section">
        <div class="scroll-wrap">
            <div id="fullpage" class="scroll-content">

                <?php $background = get_field('background_image', $post->ID);?>

                <div class="section project-detail" 
                    data-background="image" 
                    data-page="1" 
                    style="background: url(<?php echo $background['url'];?>) no-repeat; background-size: cover;">

                    <div class="slide profile-slide" 
                        data-background="image" 
                        style="background: url(<?php echo $background['url'];?>) no-repeat; background-size: cover;"
                        data-slide="0"
                        data-parent-page="1">

                        <div class="sub-page container" >
                            <div class="top-heading bz-font-bold">
                                <span class="text-color-1">
                                <?php echo $post->post_title ?>
                                </span>
                            </div>
                            <a href="javascript:;" id="detail_button" class="detail_button">
                                <kaart src="<?php echo get_theme_file_uri('assets/images');?>/detail_button.svg" alt="" class="kaart-responsive">
                            </a>
                        </div>

                    </div>

                    <?php
                        renderHTML($section, 0, 1);
                    ?>

                </div>

            </div>
        </div>
    </div>
</div>
</article>

<?php
    manualEmbedScript('project-detail');
?>