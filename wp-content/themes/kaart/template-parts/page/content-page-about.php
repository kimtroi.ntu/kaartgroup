<?php
/**
 * Created by PhpStorm.
 * User: trathailoi
 * Date: 5/3/18
 * Time: 7:19 PM
 */
?>

    <div id="mod-about">

        <section class="top-section">

			<?php $groundTruthing = get_field('ground_truthing') ?>
			<?php $gisProfessionals = get_field('gis_professionals') ?>
			<?php $qualityControl = get_field('quality_control') ?>
			<?php $contentBottom = get_field('content_bottom') ?>


            <div class="container top-section-container">
                <p class="description text-center">
					<?php echo get_field('content') ?>
                </p>
            </div>

        </section>

        <section class="middle-section">

            <div class="middle-section-list">

                <div class="middle-section-item middle-section-item-01">

                    <div class="middle-section-item-small">

                        <div class="middle-section-img-content">

                            <div class="middle-section-img">
                                <img src="<?php echo $groundTruthing['left_side']['image'] ?>" alt="" class="img-responsive">
                            </div>

                            <p class="middle-section-img-title text-center">
								<?php echo $groundTruthing['left_side']['title'] ?>
                            </p>

                        </div>

                    </div>

                    <div class="middle-section-item-large">

                        <div class="middle-section-content" style="background: url('<?php echo $groundTruthing['right_side']['background'] ?>') center center / cover no-repeat">

                            <p class="description">
								<?php echo $groundTruthing['right_side']['content'] ?>
                            </p>

                        </div>

                    </div>

                </div>

                <div class="middle-section-item middle-section-item-02">

                    <div class="middle-section-item-large">

                        <div class="middle-section-content" style="background: url('<?php echo $gisProfessionals['left_side']['background'] ?>') center center / cover no-repeat">

                            <p class="description">
								<?php echo $gisProfessionals['left_side']['content'] ?>
                                <a href="" class="learn-more-btn">
                                <span>
                                    <?php echo $gisProfessionals['left_side']['button'] ?>
                                </span>
                                </a>
                            </p>

                        </div>

                    </div>

                    <div class="middle-section-item-small">

                        <div class="middle-section-img-content">

                            <div class="middle-section-img">
                                <img src="<?php echo $gisProfessionals['right_side']['image'] ?>" alt="" class="img-responsive">
                            </div>

                            <p class="middle-section-img-title text-center">
								<?php echo $gisProfessionals['right_side']['title'] ?>
                            </p>

                        </div>

                    </div>

                </div>

                <div class="middle-section-item middle-section-item-01">

                    <div class="middle-section-item-small">

                        <div class="middle-section-img-content">

                            <div class="middle-section-img">
                                <img src="<?php echo $qualityControl['left_side']['image'] ?>" alt="" class="img-responsive">
                            </div>

                            <p class="middle-section-img-title text-center">
								<?php echo $qualityControl['left_side']['title'] ?>
                            </p>

                        </div>

                    </div>

                    <div class="middle-section-item-large">

                        <div class="middle-section-content" style="background: url('<?php echo $qualityControl['right_side']['background'] ?>') center center / cover no-repeat">

                            <p class="description">
								<?php echo $qualityControl['right_side']['content'] ?>
                            </p>

                        </div>

                    </div>

                </div>


            </div>

        </section>

        <section class="bottom-section">

            <div class="container bottom-section-container">

                <p class="description text-center">
					<?php echo $contentBottom['content']?>
                </p>

                <div class="map-img">
                    <img src="<?php echo $contentBottom['image'] ?>" alt="" class="img-responsive img-dk">
                    <img src="<?php echo $contentBottom['image_mobile'] ?>" alt="" class="img-responsive img-mb">
                </div>

            </div>

        </section>

    </div>

    </div><!-- #post-## -->

<?php
manualEmbedScript('about');
?>