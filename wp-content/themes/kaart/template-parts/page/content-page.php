<?php
/**
 * Created by PhpStorm.
 * User: trathailoi
 * Date: 5/3/18
 * Time: 8:10 AM
 */

include 'section-layouts/index.php';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="sub-page-section-wrap">
        <div class="sub-page-section">
            <div class="scroll-wrap">
                <div id="fullpage" class="scroll-content">
                    
                    <?php renderSections($post); ?>

                </div>
            </div>
            <?php renderSectionDiagram($post) ?>
        </div>
    </div>
</article><!-- #post-## -->