<?php
/**
 * Created by PhpStorm.
 * User: Mitsuha
 * Date: 5/25/2018
 * Time: 10:02 AM
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'kaart-panel ' ); ?> >

	<div id="mod-team">

		<section class="our-team">

			<div class="container our-team-container">
                <?php $our_team = get_field('our_team'); ?>
				<div class="our-team-content">

					<p class="title text-center">
                        <?php echo $our_team['header']; ?>
					</p>

					<p class="description text-center">
                        <?php echo $our_team['content']; ?>
					</p>

				</div>

				<div class="our-team-list">
                    <?php $list_member = get_field('list_member'); ?>
                    <?php foreach ($list_member as $member) { ?>
                        <?php $member = $member['member']; ?>
                        <div class="our-team-item">
                            <div class="our-team-item-container">

                                <div class="our-team-item-img" style="background: url('<?php echo $member['image']; ?>') center center / cover no-repeat">

                                    <img src="http://via.placeholder.com/370x245" alt="" class="img-responsive opacity">

                                </div>

                                <div class="our-team-item-content">

                                    <p class="name-job">

                                        <span class="name"><?php echo $member['name']; ?></span>
                                        <span class="show-dk line-hori"></span>
                                        <br class="show-mb"> <span class="job"><?php echo $member['job']; ?></span>
                                        <span class="name-job-line"></span>

                                    </p>
                                    <p class="description">
                                        <?php echo $member['description']; ?>
                                    </p>

                                </div>

                            </div>

                        </div>
                    <?php } ?>
				</div>

			</div>

		</section>



	</div>

	</div><!-- #post-## -->


<?php
    // manualEmbedScript('team');
