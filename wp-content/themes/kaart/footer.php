<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage kaart_Holding
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

	</div><!-- .site-content-contain -->

    <section class="how-can-we">
        <div class="how-can-we-bg" style="background: url('<?php echo get_field('contact_background') ?>') center center / cover no-repeat"></div>
        <div class="overplay"></div>
        <div class="container how-can-we-container">

            <p class="title text-center">
	            <?php echo get_field('contact_title') ?>
            </p>

            <div class="how-can-we-content">

                <?php echo do_shortcode(get_field('shortcode')) ?>

            </div>

        </div>

    </section>

    <footer id="mod-footer" class="site-footer" role="contentinfo">

        <div class="container footer-container">

            <div class="footer-content">

                <div class="footer-info-left">

                    <div class="logo">
                        <img src="<?php echo get_field('footer_logo','option') ?>" alt="" class="img-responsive">
                    </div>
                    
                    <?php if ( have_rows('social_networks', 'option') ) : ?>
                    <div class="social-networks">
                        <ul>
                            <?php while ( have_rows('social_networks', 'option')) : the_row(); $icon = get_sub_field('icon'); ?>
                                <li>
                                    <a href="<?= get_sub_field('url') ?>">
                                        <img src="<?= $icon['url'] ?>" height="18" alt="<?= $icon['title'] ?>">
                                    </a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                    <?php endif; ?>

	                <?php $linkButton = get_field('link_button','option') ?>

                </div>

                <div class="footer-info-right">

                    <!-- <div class="line">
                        <span></span>
                    </div> -->

                    <a href="https://www.google.com/maps/search/?api=1&query=<?php echo get_field('footer_address','option') ?>" target="_blank" class="footer-info-address">
	                    <?php echo get_field('footer_address','option') ?>
                    </a>

                </div>

                <p class="footer-info-copy-right">
                    <?php echo get_field('footer_copyright','option') ?>
                </p>

            </div>

        </div>

        <div class="wrap">

            <?php
            get_template_part( 'template-parts/footer/footer', 'widgets' );

            if ( has_nav_menu( 'social' ) ) : ?>
                <nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'kaart' ); ?>">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'social',
                        'menu_class'     => 'social-links-menu',
                        'depth'          => 1,
                        'link_before'    => '<span class="screen-reader-text">',
                        'link_after'     => '</span>' . kaart_get_svg( array( 'icon' => 'chain' ) ),
                    ) );
                    ?>
                </nav><!-- .social-navigation -->
            <?php endif;

            //				get_template_part( 'template-parts/footer/site', 'info' );
            ?>
        </div><!-- .wrap -->
    </footer><!-- #colophon -->

</div><!-- #page -->



<?php wp_footer(); ?>

</body>
</html>
