<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage kaart_Holding
 * @since 1.0
 * @version 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css?family=Hind+Guntur" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo get_field( 'favicon', 'option' )['url']; ?>" type="image/x-icon">


	<?php wp_head(); ?>


    <style type="text/css">
        .crossbrowser-filter-blur {
            -webkit-filter: blur(3px);
            -moz-filter: blur(3px);
            -ms-filter: blur(3px);
            -o-filter: blur(3px);
            /* FF doesn't support blur filter, but SVG */
            filter: url("data:image/svg+xml;utf8,<svg height='0' xmlns='http://www.w3.org/2000/svg'><filter id='svgBlur' x='-5%' y='-5%' width='110%' height='110%'><feGaussianBlur in='SourceGraphic' stdDeviation='5'/></filter></svg>#svgBlur");
            filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='3');
            filter: blur(3px);
        }
    </style>

</head>

<body <?php body_class(); ?>>

<div id="page" class="site">
    <!--	<a class="skip-link screen-reader-text" href="#content">-->
	<?php //_e( 'Skip to content', 'kaart' ); ?><!--</a>-->

    <header id="mod-nav" class="site-header" role="banner">

        <div class="container nav-container">

            <div class="logo">

                <a class="logo-link" href="<?php echo site_url(); ?>">
                    <img src="<?php echo get_field( 'logo', 'option' )['url']; ?>" alt="" class="img-responsive">
                </a>

            </div>

			<?php if ( has_nav_menu( 'top' ) ) :  //&& get_field('is_use_master_page') ?>

                <div class="nav-menu">

                    <div class="menu-list">

						<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>

                    </div><!-- .wrap -->

                </div><!-- .navigation-top -->

			<?php endif; ?>

            <div class="hamburger-btn">
                <div></div>
                <div></div>
                <div></div>
            </div>

        </div>

		<?php if ( has_nav_menu( 'top' ) ) :  //&& get_field('is_use_master_page') ?>

            <div class="nav-menu nav-menu-mb">

                <div class="menu-list">

					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>

                </div><!-- .wrap -->

            </div><!-- .navigation-top -->

		<?php endif; ?>

    </header><!-- #masthead -->

    <div class="padding-top"></div>


	<?php $banner = get_field( 'banner_pages' ); ?>
	<?php $bannerTitle = get_field( 'headline' ); ?>

	<?php if ( isset( $banner ) ) { ?>
        <section class="banner">

            <img src="http://via.placeholder.com/1440x700" alt="" class="img-responsive img-banner opacity">

			<?php if ( $banner['background_type'] == 'video' ) { ?>

				<?php if ( $banner['video']['video_type'] == 'url' ) { ?>

                    <iframe class="banner-layer-video" src="<?php echo $banner['video']['video_url'] ?>" frameborder="0"
                            allow="autoplay; encrypted-media" allowfullscreen></iframe>

				<?php } elseif ( $banner['video']['video_type'] == 'upload_file' ) { ?>

                    <video autoplay muted loop class="banner-layer-video">
                        <source src="<?php echo $banner['video']['video_file'] ?>" type="video/mp4">
                    </video>

				<?php } ?>


			<?php } elseif ( $banner['background_type'] == 'image' ) { ?>

                <div class="banner-layer-img"
                     style="background: url('<?php echo $banner['image'] ?>') center center / cover no-repeat"></div>

			<?php } ?>

            <div class="banner-container">
				<?php if ( $banner['headline']['headline_content'] == '' ) { ?>

				<?php } else { ?>
                    <div class="title-container">

                        <h1 class="title" style="background: <?php echo $banner['headline']['headline_background_color']; ?>">
                            <span><?php echo $banner['headline']['headline_content']; ?></span>
                        </h1>

                    </div>
				<?php } ?>

            </div>
        </section>

	<?php } else { ?>

	<?php } ?>


    <!--
    Start: Switch Banner

    <?php if ( $banner['background_type'] == 'image' ) { ?>

        <?php echo $banner['image']; ?>

    <?php } ?>

    <?php if ( $banner['background_type'] == 'video' ) { ?>

        <?php if ( $banner['video']['video_type'] == 'url' ) { ?>

            <?php echo $banner['video']['video_url']; ?>

        <?php } ?>

        <?php if ( $banner['video']['video_type'] == 'upload_file' ) { ?>

            <?php echo $banner['video']['video_file']; ?>

        <?php } ?>

    <?php } ?>

    End: Switch Banner
    -->

	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( is_page() && ! kaart_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
//		echo '<div class="single-featured-image-header">';
//		echo get_the_post_thumbnail( get_queried_object_id(), 'kaart-featured-image' );
//		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

    <div class="site-content-contain">
        <div id="content" class="site-content">
